#include "PatternSearchingUsingTrieSolution.h"

int main() {

    SuffixTrie* suffixTrieInstance = SuffixTrie::getInstance();
    string text;
    string pattern;
    bool isValid = false;


    cout << "Please enter a text: \n";
    while (text.length() == 0 || !isValid) {
        getline(cin, text);
        isValid = suffixTrieInstance->validateTheStringLength(text);
        if (text.length() == 0 || !isValid) {
            cout << "Empty or invalid text entered, please enter a valid text\n";
        }
    }

    
    cout << "Please enter the pattern you need to search for: \n";
    while (pattern.length() == 0 || !isValid) {
        getline(cin, pattern);
        isValid = suffixTrieInstance->validateTheStringLength(pattern);
        if (pattern.length() == 0 || !isValid) {
            cout << "Empty or invalid pattern entered, please enter a valid pattern\n";
        }
    }
    
    suffixTrieInstance->setText(text); //the whole text

    suffixTrieInstance->initializeSuffixTrie(); // fill the suffix trie with null nodes of text length

    suffixTrieInstance->search(pattern); // search the given pattern in the trie search tree

    return 0; 
}