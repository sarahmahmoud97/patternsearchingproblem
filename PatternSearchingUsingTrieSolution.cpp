#include <list> 
#include <iterator>
#include "PatternSearchingUsingTrieSolution.h"

/**
    This program represents a string pattern searching program using a Suffix Trie data structure with time complexity O(n+m) in the worst case,
    where n is the text length, and m is the pattern length
*/

/* Null, because instance will be initialized on demand. */
SuffixTrie* SuffixTrie::instance = NULL;

/*default private constructor*/
SuffixTrie::SuffixTrie() {}

/*This public method return a single instance of that class because we don't more than one instance "Singelton design pattern"*/
SuffixTrie* SuffixTrie::getInstance()
{
    if (instance == NULL)
    {
        instance = new SuffixTrie();
    }

    return instance;
}

/*This method validates that the string length will not exceed 1024 chars*/
bool SuffixTrie::validateTheStringLength(string text) {
    if (text.length() > MAX_CHARS) {
        try {
            throw ExceedsStringLength();
        } catch(ExceedsStringLength& e) {
            cout << "ExceedsStringLength Exception caught" << endl;
            cout << e.what() << endl;
        } catch(exception& e) {
           cout << e.what() << endl;
        }
        return false;
    } else {
        return true;
    }
}

void SuffixTrie::setText(string text) 
{ 
    bool isValid = SuffixTrie::validateTheStringLength(text);
    if (isValid) {
        this->text = text;
    }
}

string SuffixTrie::getText() 
{ 
    string copytext = this->text;
    return copytext;
}
 
void SuffixTrieNode::insertSuffix(string suffix, int index) 
{ 
    // Store index in linked list 
    indexes->push_back(index); 

    // If string has more characters 
    if (suffix.length() > 0) 
    { 
        // Find the first character 
        char suffixFirstChar = suffix.at(0); 

        // If there is no edge for this character, add a new edge 
        if (children[suffixFirstChar] == NULL) 
            children[suffixFirstChar] = new SuffixTrieNode(); 

        // Recur for next suffix 
        children[suffixFirstChar]->insertSuffix(suffix.substr(1), index+1); 
    } 
}

void SuffixTrie::initializeSuffixTrie() { //It builds a trie of suffixes of the given text
    for (int i = 0; i < this->text.length(); i++) 
        root.insertSuffix(this->text.substr(i), i);
}

// A recursive function to search a pattern in subtree rooted with 
// this node 
list<int>* SuffixTrieNode::search(string pattern) 
{ 
    // If all characters of pattern have been processed, 
    if (pattern.length() == 0) 
        return indexes; 

    // if there is an edge from the current node of suffix trie, 
    // follow the edge. 
    if (children[pattern.at(0)] != NULL) 
        return (children[pattern.at(0)])->search(pattern.substr(1)); 

    // If there is no edge, pattern doesn’t exist in text 
    else return NULL; 
} 

/* Prints all occurrences of pat in the Suffix Trie S (built for text)*/
void SuffixTrie::search(string pattern) 
{ 
    list<int> *indexesOfMatchedSuffixes = root.search(pattern); 

    // Check if the list of indexes is empty or not 
    if (indexesOfMatchedSuffixes == NULL || indexesOfMatchedSuffixes->empty()) //the list should be null in this case but for extra check i checked the empty list
        cout << "Pattern not found" << endl; 
    else { 
        int counter = 0;
        list<int>::iterator it; 
        int patternLength = pattern.length();
        string patternFound = pattern + " is found at index ";

        for (it = indexesOfMatchedSuffixes->begin(); it != indexesOfMatchedSuffixes->end(); ++it) {
            counter++;
            patternFound+= to_string(*it - patternLength);
            if (counter < indexesOfMatchedSuffixes->size()) {
                patternFound += ", ";
            } else {
                patternFound += ".";
            }
        }

        cout << patternFound << endl;
    }
}