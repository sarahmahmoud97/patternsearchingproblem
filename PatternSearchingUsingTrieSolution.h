#pragma once
#include<iostream> 
#include <exception>
using namespace std;
#include <string>
#include <list>
#define MAX_CHARS 1024 


/*Due to single resposiblity pattern (SOLID Patterns), each class will have only one resposiblity */
struct ExceedsStringLength : public exception {
   const char * what () const throw () {
      return "Exceeds String Length Exception, the length of text should be <= 1024";
   }
};

/*This class used to initial each node in the suffix tree (includes root and it's childerns) */
class SuffixTrieNode { 
private: 
	SuffixTrieNode *children[MAX_CHARS]; 
	list<int> *indexes; 
public: 
	SuffixTrieNode()
	{ 
		indexes = new list<int>; 

		// Initialize all child pointers as NULL 
		for (int i = 0; i < MAX_CHARS; i++) {
		    children[i] = NULL;
		}
	} 

	~SuffixTrieNode()
	{
		delete indexes;
		delete children;
	}

	void insertSuffix(string suffix, int index); 

	// returned list containing all indexes where pattern is present. 
	// The returned indexes are indexes of last characters of matched text, so that to get the actual index we need to subtract the pattern length.
	list<int>* search(string pat); 
}; 

// A Trie of all suffixes 
class SuffixTrie 
{ 
private: 
	SuffixTrieNode root;
	string text;
	static SuffixTrie* instance;
	SuffixTrie();
public:
    string getText();
	void setText(string text);
	bool validateTheStringLength(string text);
	static SuffixTrie* getInstance();
	void initializeSuffixTrie();

	// Function to search a pattern in the suffix trie built from the text. 
	void search(const string pattern); 

	~SuffixTrie()
	{
		delete instance;
	}
};
